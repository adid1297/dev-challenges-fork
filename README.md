# LKG Web Development Challenges

## Getting Prepared

**NOTE:** For all of our challenges, you will need to work locally and you will need to have provided us with a BitBucket username. If you don't have a BitBucket username, please register for one for free from https://bitbucket.org/account/signup/ . Please ensure that you are running on the latest production version of the language(s) and technologies associated with your challenge.

1. Clone this repository 
2. Create a new branch off of the master branch
3. Name the branch your first and last name, separated with a "-" e.g. steve-smith
4. Check your new branch
5. Commit all of your changes as you go, including clear and concise commit messages
6. Once you have completed your work, push your branch to the repository
