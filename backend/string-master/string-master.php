<?php

class StringMaster {

  /*

  When the String Master receives a string, they will attempt to create words
  out of the letters in the string with the following conditions:

  * The letters do not have to be in order, they can be rearranged but the same
    string cannot be used more than once.
  * The string can be reused for as many iterations as possible before a duplicate
    string is inputed.
  * If the string can create words which match words in the master list, the player
    will rack up points which are added to the player's final score.
  * The points are calculated based on the number of letters in each matching word.
  * Points are not given for fragments of words from the master list.
  * Not all letters in the player's inputted string have to be used for each attempt.
  * The player inputted string cannot be more than 20 characters, the string master
    doesn't have time for such shenanigans.
  * Here's some example string scores for the player inputted string "futurespeak" :
  ** as : 2
  ** easer : 5
  ** auteurs : 7
  ** rear : 0 - there is only 1 r in the player inputed string
  ** bark : 0 - there is no b in the player inputed string

  Once the String Master has processed the player inputted string following the above conditions,
  the user will be provided with a final score. It's up to the String Master to determine how high
  the score should be to beat the String Master....in case you hadn't guessed, you will be developing
  the String Master so it's up to you to decide what the high score should be to beat the String Master.
  The user should be able to both input a string and return a result, how this is handled is up to you.

  */

	function __construct() {

  }

  function submitString() {
    // Submit player inputted string
  }

	function getPoints() {
    // Calculate player's points
  }

  function checkIfWinner() {
    // Check if player has beaten the String Master's high score
  }


}

?>
