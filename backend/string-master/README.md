# LKG Web Development Challenge - The String Master

**Please ensure that you read all instructions thoroughly before proceeding with this challenge. For instructions on preparing your workspace for our challenges, please ensure you review the readme in the root of this repo https://bitbucket.org/lkg/dev-challenges**

## The Challenge

Your challenge, should you choose to accept it, is to develop a game in the form of a PHP application.

To play the game, players must input a string which is run past the _String Master_. The _String Master_ has a substantial array of strings which they will assess inputed string against and provide a score at the end; the higher the score the higher the chance the player has of beating the _String Master_!

When the _String Master_ receives a string, they will attempt to create words out of the letters in the inputted string with the following conditions:

* The letters do not have to be in order, they can be rearranged but the same string cannot be used more than once.
* The string can be reused for as many iterations as possible before a duplicate string is inputed.
* If the string can create words which match words in the master list, the player will rack up points which are added to the player's final score.
* The points are calculated based on the number of letters in each matching word.
* Points are not given for fragments of words from the master list.
* Not all letters in the player's inputted string have to be used for each attempt.
* The player inputted string cannot be more than 20 characters, the _String Master_ doesn't have time for such shenanigans.  
* Here's some example string scores for the player inputted string "_futurespeak_" :
    * as : 2
    * easer : 5
    * auteurs : 7
    * rear : 0 - there is only 1 r in the player inputed string
    * bark : 0 - there is no b in the player inputed string

Once the _String Master_ has processed the player inputted string following the above conditions, the user will be provided with a final score. It's up to the _String Master_ to determine how high the score should be to beat the _String Master_....in case you hadn't guessed, you will be developing the _String Master_ so it's up to you to decide what the high score should be to beat the _String Master_. The user should be able to both input a string and return a result, how this is handled is up to you.

### Challenge Details
* A sample file to get you started can be found in this directory backend/string-master/string-master.php . This is purely a sample, you can modify this file or add new files / directories as you see fit.
* This challenge should take no more than 3 hours; please try to complete your challenge within this time.
* This challenge is designed to test your PHP skills rather than how you store data so there is no need to build a database to store the words submitted by players.
* Please leave detailed comments alongside your code so that we can see your thought process.
* If you're unsure of any of these instructions, please make a reasonable assumption and document your assumption in the code with a comment. If you wish to provide more information, please do so in a ReadMe.txt document.
