# LKG JavaScript Development Challenge - ES6 JS Based Components

**Please ensure that you read all instructions thoroughly before proceeding with this challenge. For instructions on preparing your workspace for our challenges, please ensure you review the readme in the root of this repo https://bitbucket.org/lkg/dev-challenges**

## Challenge Introduction
The challenge's main goal is to evaluate your Javascript knowledge, problem solving, and practical development skills.
Your challenge is to add functionality to a component based Single Page Application (SPA) based on JavaScript. Details can be found inside the relevant JS files in the repository.


## Challenge Instructions
The challenge is divided into 5 main sections, as outlined below. To get started, explore all files inside the /js folder.
Each challenge section includes a relevant js file, with specific details inside the file as comments.
For example, all Weather work should be placed inside /js/weather.js.
Once all sections below are completed, please edit main.js to draw the components inside the correct div container.
The index.html file should not be modified.

### First Step
- [x] Read the Instructions

### 1. Weather
- [ ] Fetch Weather Data
- [ ] Display Temperature
- [ ] Change Image based on weather condition
- [ ] Display condition text

### 2. News
- [ ] Fetch News Data
- [ ] Display News Title
- [ ] Display News Body
- [ ] Fetch Tags Data
- [ ] Join news and tags data
- [ ] Display tags

### 3. Form
- [ ] Validate email input in JS
- [ ] Submit email via Fetch API with POST
- [ ] Display response to the user on using (alert.js) file
- [ ] Alert popup should be hidden after a few seconds
- [ ] Clear email field after submission

### 4. Time Manipulation
- [ ] Get current time
- [ ] Store data on the browser (choose any way to store, cookies, localstorage, indexedb etc)
- [ ] Check if data exists on the browser
- [ ] Calculate time difference
- [ ] Display time difference in the format: "First Vist: 2 Minutes ago" for example

## 5. Additional Details
* You will have to work with the files provided. Please do not modify index.html file.
* Feel free to create new files as you see fit. Do not use external libraries.
* Feel free to use any front-end technologies that you prefer to work with and try to keep the time that you spend on this challenge below 3 hours.
* Please leave detailed comments alongside your code so that we can see your thought process
* If you're unsure of any of these instructions, please make a reasonable assumption and document your assumption in the code with a comment. If you wish to provide more information, please do so in a ReadMe.txt document.