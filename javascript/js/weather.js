// Make this component data driven from an external weather API. It supports the following weather conditions:
// - Sunny
// - Rainy
// - Cloudy
// The API data should modify the image, condition text, and temperature text in the widget below.
// The following endpoint must be used, and will return the weather data Object:
// https://ms.lkgtools.net/v1/devchallenge/iuhewrfiuyh97h/?type=weather
// Fetch data from the API and display the relevant data in the widget below.

let Weather = '21'
let Condition = 'Sunny'
let WeatherWidget = `
<div class="max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl mt-16">
  <div class="flex-shrink-0">
    <img class="h-24 w-24" src="img/sunny.svg" alt="sunny">
  </div>
  <div class="ml-6 pt-1">
    <h4 class="text-5xl text-gray-900 leading-tight font-bold">${Weather}<img class="h-12 w-12 inline mb-4" src="img/celcius.svg" alt="Sunny"></h4>
    <p class="text-base text-gray-600 leading-normal">Today's Weather is ${Condition}</p>
  </div>
</div>`