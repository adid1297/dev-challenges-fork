// This button should trigger an event that refreshes the data on all data driven components on the page

let RefreshButton = '<button class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded absolute top-0 right-0">Refresh</button>';