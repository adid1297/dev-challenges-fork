// Please fetch a news item from the API below, and add it to the NewsCard component in this file.
// https://ms.lkgtools.net/v1/devchallenge/iuhewrfiuyh97h/?type=news

// News include an array with 1 to 3 tags, that must be fetched from the following endpoint:
// https://ms.lkgtools.net/v1/devchallenge/iuhewrfiuyh97h/?type=tags
// 
// Each news item will return a set of tags (1 to 3 tags) with a value from 0 to 10. You will have to load the Tags object above, 
// and return the word for each tag, and prepend # to each tag name.
// For example, if the news returns tags: [ 4, 5, 0], the following tags should be displayed:
// #video #animation, #news
// The above tags should be displayed inside the tag area in the NewsCard component

let NewsCard = `
<div class="max-w-sm mx-auto rounded overflow-hidden shadow-lg bg-white mt-10">
  <img class="w-full" src="img/card-top.jpg" alt="Enter the article title here">
  <div class="px-6 py-4">
    <div class="font-bold text-xl mb-2">Latest News</div>
    <p class="text-gray-700 text-base">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
    </p>
  </div>
  <div class="px-6 py-4">
    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">#tag1</span>
    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">#tag2</span>
    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700">#tag3</span>
  </div>
</div>`