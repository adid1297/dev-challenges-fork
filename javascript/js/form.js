// This form should:
// - Validate the email address with JS
// - Use Fetch() API to submit the email address to via POST: 
// https://ms.lkgtools.net/v1/devchallenge/iuhewrfiuyh97h/
// - Check the endpoint response, and display a success or failure notice on the alert inside alert.js.
// The Endpoint will not save anything, so feel free to submit it many times.
// ! Email validation must be done in JS


let FormSubmit = `
<div class="max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl mt-10" class="EmailFormCard">
<form class="w-full max-w-sm" id="EmailForm">
  <div class="flex items-center border-b border-b-2 border-teal-500 py-2">
    <input class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none" type="text" placeholder="name@domain.com" aria-label="Email">
    <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">
      Sign Up
    </button>
    <button id="EmailButton" class="flex-shrink-0 border-transparent border-4 text-teal-500 hover:text-teal-800 text-sm py-1 px-2 rounded" type="button">
      Cancel
    </button>
  </div>
</form>
</div>`