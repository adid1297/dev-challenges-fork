// This file should diplay a Usage time on the bottom of the page based on the FIRST visit.
// When an user first visits the page, store data with the time the user first logged in. Then access the saved data, and compare 
// how long elapsed since the user's first visit.
// It should display the following range:
//First Visit:
// 0s to 59s - Just Now
// 1m - 59m - 2 minutes ago
// 1h - 24h - 2 hours ago
// 1d - 30d - 2 days ago
// 30 days + - ages ago

let TimeText = 'First Visit: 2 minutes ago'
let TimeWidget = `
<p class="bg-white text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow w-64 mx-auto mt-10 text-center">
  ${TimeText}
</p>`