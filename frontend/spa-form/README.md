# LKG Frontend Development Challenge - Single Page Application With Form

**Please ensure that you read all instructions thoroughly before proceeding with this challenge. For instructions on preparing your workspace for our challenges, please ensure you review the readme in the root of this repo https://bitbucket.org/lkg/dev-challenges**

## Challenge Introduction
Your challenge is to create a SPA containing 1 form which doesn't need to submit anywhere but all inputs in the form must be validated on submission as defined later in this brief. Save your data to a client-side storage of choice, this challenge is testing your Frontend Developer skills. This challenge is designed to give you the opportunity to show off your Front End Developer skillset and prove that you've got what it takes to work with us. 

### 1. Form Creation
* **First Name** - _text(max-length 20)_
* **Last Name** - _text(max-length 20)_ 
* **Nickname** - _text(max-length 20)_ 
* **Website** - _text(max-length 20)_
* **Postcode** - _number(max-length 4)_
* **I accept terms & conditions** - _checkbox_
* **Submit** - _button_

### 2. Form Validation
Each of the form fields will need to be required and the following error messages should appear if the fields are missing inputs:

* **First Name:** Please enter your first name to proceed
* **Last Name:** Please enter your last name to proceed_
* **Nickname:** Please enter a nickname you prefer to be called_
* **Website:** Please enter your website to proceed_
* **Postcode:** Please enter a valid postcode to proceed_
* **I accept terms & conditions:** You must accept our terms and conditions to proceed

Additional validation is required for the following fields:

* **Website:** Must be valid URL including http(s):// . 
    * _You get extra points if you can also verify that the website exists._
* **Postcode:** Must be a valid Australian Postcode, this can be verified using an external API such as http://postcodeapi.com.au/ .

### 3. Display Form Results
Display your submitted data to a page or component from your client-side storage. You can use any web storage, state management library, or your component's local data object (more efficient and eloquent way, the better). Make use of your SPA implementation for the data to display seamlessly.

### 4. Responsive Design
The form should display at **50%** of the width of the screen for viewport widths of greater than 767px; for all smaller devices the width should be **90%**. The form fields need to be responsive on a 12 column grid as follows:

* **First Name**
    * Greater than or equal to 767px - 4 columns
    * Less than 767px - 12 columns
* **Last Name**
    * Greater than or equal to 767px - 4 columns
    * Less than 767px - 12 columns
* **Nickname** 
    * Greater than or equal to 767px - 4 columns
    * Less than 767px - 12 columns
* **Website**
    * Greater than or equal to 767px - 6 columns
    * Less than 767px - 12 columns
* **Postcode**
    * Greater than or equal to 767px - 6 columns
    * Less than 767px - 12 columns
* **I accept terms & conditions** - 12 columns 
* **Submit** - 12 columns

### 5. General Styling / Beautifying 
* The last part of the challenge is to style the form & page as you see fit and remember a simple, fast and easy to use interface is our preference. 
* This would also be the time to bust out your less SCSS / LESS.

## 6. Challenge Details
* We haven't provided any sample files for you to use, you'll need to create these yourself and sure that all files are included in your commits ready for us to pull and test.
* Feel free to use any front-end technologies that you prefer to work with and try to keep the time that you spend on this challenge below 4 hours. 
* Please leave detailed comments alongside your code so that we can see your thought process
* If you're unsure of any of these instructions, please make a reasonable assumption and document your assumption in the code with a comment. If you wish to provide more information, please do so in a ReadMe.txt document.
